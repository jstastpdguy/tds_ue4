// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FuncLibrary/Types.h"
#include "InventorySystem.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentWeaponIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeWeaponUpdateWidget, int32, WeaponSlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EWeaponType, TypeAmmo, int32, Count);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, int32, IndexSlot, FAdditionalWeaponInfo, AdditionalInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoTypeEmpty, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAmmoTypeAvailable, EWeaponType, WeaponType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveNoAmmo, int32, WeaponSlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponHaveAmmo, int32, WeaponSlotIndex);

UCLASS( ClassGroup=(Custom), Blueprintable, meta=(BlueprintSpawnableComponent) )
class TDS_API UInventorySystem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventorySystem();
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnChangeWeaponUpdateWidget OnChangeWeaponUpdateWidget;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponAdditionalInfoChange OnAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoTypeEmpty OnAmmoTypeEmpty;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnAmmoTypeAvailable OnAmmoTypeAvailable;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnUpdateWeaponSlots OnUpdateWeaponSlots;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponHaveNoAmmo OnWeaponHaveNoAmmo;
	UPROPERTY(BlueprintAssignable, Category = "Inventory")
	FOnWeaponHaveAmmo OnWeaponHaveAmmo;

	UPROPERTY(Replicated, EditAnyWhere, BlueprintReadOnly, Category = "Weapons")
	TArray<FWeaponSlot> WeaponSlots;
	UPROPERTY(Replicated, EditAnyWhere, BlueprintReadOnly, Category = "Weapons")
	TArray<FAmmoSlot> AmmoSlots;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapons")
	int32 MaxSlotsWeapon = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	bool SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex);

	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 IndexWeapon);
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	void GetWeaponNameByIndex(FName& Name, int32 WeaponSlot);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo);
	
	UFUNCTION(BlueprintCallable)
	void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 ChangeAmmoValue);
	bool CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AvailableAmmo);

	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeWeapon(int32& FreeSlotIndex);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool CheckCanTakeAmmo(EWeaponType AmmoType);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool SwitchItemToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, FDropItem& DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void SaveItemToInventory_OnServer(FWeaponSlot NewWeapon, AActor* PickUpActor);
	UFUNCTION(BlueprintCallable, Category = "Interface")
	bool GetDropItemFromInventory(int32 IndexSlot, FDropItem& DropItemInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Interface")
	void DropWeaponByIndex_OnServer(int32 SlotIndex);
	
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FWeaponSlot> GetWeaponSlots();
	UFUNCTION(BlueprintCallable, Category = "Inventory")
	TArray<FAmmoSlot> GetAmmoSlots();

	//Multiplayer
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void InitInventory_OnServer(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo);
	UFUNCTION(Server, Reliable, BlueprintCallable, Category = "Inventory")
	void SwitchWeaponEvent_OnServer(FName WeaponName, FAdditionalWeaponInfo WeaponInfo, int32 NewWeaponIndex);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void UpdateWidgetByChangeWeaponEvent_Multicast(int32 NewWeaponIndex);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void AmmoChangeEvent_Multicast(EWeaponType TypeWeapon, int32 Count);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void AmmoTypeEmptyEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void AmmoTypeAvailableEvent_Multicast(EWeaponType WeaponType);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void UpdateWeaponSLotEvent_Multicast(int32 SlotIndex, FWeaponSlot NewWeapon);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void WeaponHaveNoAmmoEvent_Multicast(int32 SlotIndex);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void WeaponHaveAmmoEvent_Multicast(int32 SlotIndex);
	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Inventory")
	void WeaponAdditionalInfoChangeEvent_Multicast(int32 SlotIndex, FAdditionalWeaponInfo NewWeaponInfo);
};
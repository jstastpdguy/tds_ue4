// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "../../FuncLibrary/Types.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class TDS_API AProjectileDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectileDefault();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* MeshComp = nullptr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UParticleSystemComponent* FX = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;
	UPROPERTY(BlueprintReadOnly)
	FProjectileInfo ProjectileSettings;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void InitProjectile(FProjectileInfo InitParam);

	UFUNCTION()
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	UFUNCTION()
	void BulletCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void BulletCollisionEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void ImpactProjectile();

	UFUNCTION(NetMulticast, Reliable)
	void InitVisualMeshProjectile_Multicast(UStaticMesh* newMesh, FTransform MeshRelative);
	UFUNCTION(NetMulticast, Reliable)
	void InitVisualTrailProjectile_Multicast(UParticleSystem* newTrail, FTransform TrailRelative);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComponent, FHitResult HitRes);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitFX_Multicast(UParticleSystem* FXTempl, FHitResult HitRes);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitSound_Multicast(USoundBase* HitSound, FHitResult HitRes);
	UFUNCTION(NetMulticast, Reliable)
	void SetProjectileVelocity_Multicast(const float NewInitialSpeed, const float NewMaxSpeed);
	virtual void PostNetReceiveVelocity(const FVector& NewVelocity) override;
};
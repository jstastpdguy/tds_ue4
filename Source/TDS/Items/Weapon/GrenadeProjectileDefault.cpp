// Fill out your copyright notice in the Description page of Project Settings.

#include "GrenadeProjectileDefault.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("TDS.DebugExplode"), 
	DebugExplodeShow, 
	TEXT("Draw Debug for Explode"), 
	ECVF_Cheat);

void AGrenadeProjectileDefault::BeginPlay() {
	Super::BeginPlay();
}

void AGrenadeProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ExplosionTimeFunc(DeltaTime);
}

void AGrenadeProjectileDefault::ExplosionTimeFunc(float DeltaTime)
{
	if (TimerEnabled) {
		if (ExplosionTimer > ExplosionTime)
		{
			Explosion();
		}
		else {
			ExplosionTimer += DeltaTime;
		}
	}
}

void AGrenadeProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComponent, NormalImpulse, Hit);
	
	//Uses for RPG or something same, when need to detonate on hit
	
	if (ProjectileSettings.bShouldExplodeOnHit) {
		Explosion();
	}
}

void AGrenadeProjectileDefault::ImpactProjectile()
{
	TimerEnabled = true;
}

void AGrenadeProjectileDefault::Explosion()
{
	if (DebugExplodeShow) {
		//inner radius - max damage
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExplosionInnerRadius, 12, FColor::Red, false, 5.f, (uint8)'\000', 2.f);
		//Outer radius - max falloff
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSettings.ExplosionOuterRadius, 12, FColor::Yellow, false, 5.f, (uint8)'\000', 2.f);
	}

	TimerEnabled = false;
	if (ProjectileSettings.ExplosionFX) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSettings.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSettings.ExplosionSound) {
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSettings.ExplosionSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;

	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSettings.ExplosionMaxDamage,
		ProjectileSettings.ExplosionMaxDamage*0.2f,
		GetActorLocation(),
		ProjectileSettings.ExplosionInnerRadius,
		ProjectileSettings.ExplosionOuterRadius,
		ProjectileSettings.ExplosionDamageFalloffCoeff,
		NULL,
		IgnoredActor,
		this, nullptr);

	this->Destroy();
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "GrenadeProjectileDefault.generated.h"

UCLASS()
class TDS_API AGrenadeProjectileDefault : public AProjectileDefault
{
	GENERATED_BODY()
	
protected:
	
	virtual void BeginPlay() override;

public:
	
	virtual void Tick(float DeltaTime) override;

	void ExplosionTimeFunc(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) override;
	
	virtual void ImpactProjectile() override;

	void Explosion();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Delay")
		bool TimerEnabled = false;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Delay")
		float ExplosionTimer = 0.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Delay")
		float ExplosionTime = 5.0f;
};
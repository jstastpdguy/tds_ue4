// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSStateEffects.h"
#include "GeneratedCodeHelpers.h"
#include "../../Character/HealthComponent.h"
#include "Kismet/GameplayStatics.h"
#include "../../Interface/TDS_I_GameActors.h"

bool UTDSStateEffects::InitObject(AActor* Actor, FName HittedBone) {

	myActor = Actor;
	BoneName = HittedBone;
	
	ITDS_I_GameActors* myInterface = Cast<ITDS_I_GameActors>(myActor);
	
	if (myInterface) {
		myInterface->AddEffect(this);
	}
	
	return true;
}

void UTDSStateEffects::DestroyObject()
{
	ITDS_I_GameActors* myInterface = Cast<ITDS_I_GameActors>(myActor);
	if (myInterface) {
		myInterface->RemoveEffect(this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel()) {
		this->ConditionalBeginDestroy();
	}
}

const FName* UTDSStateEffects::GetBoneName() const{
	return &BoneName;
}

const UParticleSystem* UTDSStateEffects::GetParticleEffect() const
{
	return ParticleEffect;
}

const TArray<TEnumAsByte<EPhysicalSurface>>* UTDSStateEffects::GetPossibleInteractSurface() const
{
	return &PossibleInteractSurface;
}

bool UTDSStateEffectOnceExecution::InitObject(AActor* Actor, FName HittedBone) {

	Super::InitObject(Actor, HittedBone);
	ExecuteOnce();
	return true;
}

void UTDSStateEffectOnceExecution::DestroyObject()
{
	Super::DestroyObject();
}

void UTDSStateEffectOnceExecution::ExecuteOnce()
{
	if (myActor) {
		UHealthComponent* myHealth = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealth) {
			myHealth->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool UTDSStateEffectTimerExecution::InitObject(AActor* Actor, FName HittedBone)
{
	Super::InitObject(Actor, HittedBone);

	if(GetWorld()){
		GetWorld()->GetTimerManager().SetTimer(ExecutionTimer, this, &UTDSStateEffectTimerExecution::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(EffectTimer, this, &UTDSStateEffectTimerExecution::Execute, TimerRate, true);
	}

	return true;
}

void UTDSStateEffectTimerExecution::DestroyObject()
{
	if(GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	Super::DestroyObject();
}

void UTDSStateEffectTimerExecution::Execute()
{
	if (myActor) {
		UHealthComponent* myHealth = Cast<UHealthComponent>(myActor->GetComponentByClass(UHealthComponent::StaticClass()));
		if (myHealth) {
			myHealth->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UTDSStateEffects::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(UTDSStateEffects, BoneName);
}
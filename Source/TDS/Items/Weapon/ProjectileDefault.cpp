// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AISense_Damage.h"

// Sets default values
AProjectileDefault::AProjectileDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);
	BulletCollisionSphere->bReturnMaterialOnMove = true; //hit event return phys material
	BulletCollisionSphere->SetCanEverAffectNavigation(false); //collision not affect navigation (p button at editor)
	BulletCollisionSphere->SetCollisionProfileName(TEXT("Projectile"));

	RootComponent = BulletCollisionSphere;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	MeshComp->SetupAttachment(RootComponent);
	MeshComp->SetCanEverAffectNavigation(false);
	MeshComp->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel2);

	FX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	FX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Projectile Movement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;
	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
	//BulletProjectileMovement->bAutoActivate = false;
	
	SetReplicates(true);
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionEndOverlap);

}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//called before construction script and begin play
void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	this->SetLifeSpan(InitParam.ProjectileLifeTime);
	SetProjectileVelocity_Multicast(InitParam.ProjectileInitSpeed, InitParam.ProjectileMaxSpeed);
	
	if (InitParam.BulletMesh) {
		InitVisualMeshProjectile_Multicast(InitParam.BulletMesh, FTransform(FQuat(FRotator(0, -90, 0))));
	}
	else {
		MeshComp->DestroyComponent();
	}
	
	if (InitParam.BulletFX) {
		InitVisualTrailProjectile_Multicast(InitParam.BulletFX, InitParam.FXOffset);
	}
	else {
		FX->DestroyComponent();
	}

	ProjectileSettings = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface MySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSettings.HitDecals.Contains(MySurfacetype))
		{
			UMaterialInterface* MyMaterial = ProjectileSettings.HitDecals[MySurfacetype];

			if (MyMaterial && OtherComponent)
			{
				SpawnHitDecal_Multicast(MyMaterial, OtherComponent, Hit);
			}
		}
		if (ProjectileSettings.HitFX.Contains(MySurfacetype))
		{
			UParticleSystem* MyParticle = ProjectileSettings.HitFX[MySurfacetype];
			if (MyParticle)
			{
				SpawnHitFX_Multicast(MyParticle, Hit);
			}
		}

		if (ProjectileSettings.HitSound)
		{
			SpawnHitSound_Multicast(ProjectileSettings.HitSound, Hit);
		}

		if(ProjectileSettings.Effect){
		
			UTypes::AddEffectBySurfaceType(OtherActor, ProjectileSettings.Effect, MySurfacetype, Hit.BoneName);
		}
	}

	UGameplayStatics::ApplyDamage(OtherActor, ProjectileSettings.ProjectileDamage, GetInstigatorController(), this, NULL);
	UAISense_Damage::ReportDamageEvent(GetWorld(), Hit.GetActor(), GetInstigator(), ProjectileSettings.ProjectileDamage, Hit.Location, Hit.Location); //TODO shotgun, grenades

	ImpactProjectile();
}

void AProjectileDefault::BulletCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
}

//destroy itself on collision hit
void AProjectileDefault::ImpactProjectile()
{
	this->Destroy();
}

void AProjectileDefault::PostNetReceiveVelocity(const FVector& NewVelocity)
{
	if(BulletProjectileMovement)
	{
		BulletProjectileMovement->Velocity = NewVelocity;
	}
}

void AProjectileDefault::SetProjectileVelocity_Multicast_Implementation(const float NewInitialSpeed, const float NewMaxSpeed)
{
	if(BulletProjectileMovement){
		BulletProjectileMovement->InitialSpeed = NewInitialSpeed;
		BulletProjectileMovement->MaxSpeed = NewMaxSpeed;
		BulletProjectileMovement->Velocity = GetActorForwardVector() * NewInitialSpeed;
	}
	/*const FVector NewVelocity = FVector::ForwardVector * BulletProjectileMovement->InitialSpeed;
	
	BulletProjectileMovement->SetVelocityInLocalSpace(NewVelocity);
	BulletProjectileMovement->SetActive(true, true);*/
}

void AProjectileDefault::SpawnHitDecal_Multicast_Implementation(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComponent, FHitResult HitRes)
{
	UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(10.0f), OtherComponent, NAME_None, HitRes.ImpactPoint, HitRes.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
}

void AProjectileDefault::SpawnHitFX_Multicast_Implementation(UParticleSystem* FXTempl, FHitResult HitRes)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXTempl, FTransform(HitRes.ImpactNormal.Rotation(), HitRes.ImpactPoint, FVector(1.0f)));
}

void AProjectileDefault::SpawnHitSound_Multicast_Implementation(USoundBase* HitSound, FHitResult HitRes)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitSound, HitRes.ImpactPoint);
}

void AProjectileDefault::InitVisualMeshProjectile_Multicast_Implementation(UStaticMesh* newMesh, FTransform MeshRelative)
{
	MeshComp->SetStaticMesh(newMesh);
	MeshComp->SetRelativeTransform(MeshRelative);
}

void AProjectileDefault::InitVisualTrailProjectile_Multicast_Implementation(UParticleSystem* newTrail, FTransform TrailRelative)
{
	FX->SetTemplate(newTrail);
	FX->SetRelativeTransform(TrailRelative);
}

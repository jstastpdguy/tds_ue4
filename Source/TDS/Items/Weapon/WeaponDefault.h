// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "../../FuncLibrary/Types.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadStart, UAnimMontage*, AnimReloadChar, float, ReloadTime);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY()
	FName CurrentWeaponName;
	UPROPERTY()
	FWeaponInfo WeaponSettings;
	UPROPERTY(Replicated, EditAnyWhere, BlueprintReadWrite, Category = "WeaponInfo")
	FAdditionalWeaponInfo WeaponInfo;

	//flags
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Debug")
	bool ShowDebug = false;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "WeaponState")
	bool WeaponFiring = false;
	UPROPERTY(Replicated, EditAnyWhere, BlueprintReadOnly, Category = "WeaponState")
	bool WeaponReloading = false;
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "WeaponState")
	bool WeaponAiming = false;

	bool bDropClip = false;
	bool bDropBulletShell = false;
	bool BlockFire = false;
	UPROPERTY(Replicated)
	bool ShouldReduceDispersion = true;

	//Time management
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;
	float FireTimer = 0.0f;
	float DropClipTimer = -1.f;
	float DropShellTimer = -1.f;

	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ReloadLogic")
	float SizeVectorToChangeShootDirectionLogic = 100.0f;
	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USkeletalMeshComponent* SkeletalMesh = nullptr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMesh = nullptr;
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UArrowComponent* ShootLocation = nullptr;

public:	

	UFUNCTION(BlueprintCallable, BlueprintPure)
	float GetWeaponReloadTime();
	UFUNCTION(Server, Reliable)
	void InitDropMesh_OnServer(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDir, float MeshLifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	
	float GetWeaponShootingZCoord();

	void WeaponInit();
	FProjectileInfo GetProjectile();
	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponClipSize();

	// Ticks
	virtual void Tick(float DeltaTime) override;
	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);
	void ClipDropTick(float DeltaTime);

	void InitReload();
	void FinishReload();
	void CancelReload();

	int32 GetAvailableAmmoForReload();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponFireState_OnServer(bool bIsFire);
	bool CheckWeaponCanFire() const;
	bool CanWeaponReload();
	void Fire();
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;
	EWeaponType GetWeaponType() const;

	UFUNCTION(BlueprintCallable)
	float WeaponShootDistance();

	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool NewShouldReduceDispersion);
	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponStart_Multicast(UAnimMontage* Anim, float PlayRate);
	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFire_Multicast(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDir, float MeshLifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir);
	UFUNCTION(NetMulticast, Reliable)
	void FXWeaponFire_Multicast(UParticleSystem* FXFire, USoundBase* SoundFire);
};
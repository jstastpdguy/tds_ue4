// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDSStateEffects.generated.h"

UCLASS(Blueprintable, BlueprintType)
class TDS_API UTDSStateEffects : public UObject
{
	GENERATED_BODY()
	
protected:

	AActor* myActor = nullptr;

	virtual bool IsSupportedForNetworking() const override { return true; };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStackable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(Replicated)
	FName BoneName = NAME_None;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsAutoDestroyParticle = false;

public:
	virtual bool InitObject(AActor* Actor, FName HittedBone);
	virtual void DestroyObject();

	//getters
	bool IsStackable() const { return bIsStackable; };
	const FName* GetBoneName() const;
	const UParticleSystem* GetParticleEffect() const;
	const TArray<TEnumAsByte<EPhysicalSurface>>* GetPossibleInteractSurface() const;
	bool IsAutoDestroyParticle() const { return bIsAutoDestroyParticle; };
};

UCLASS()
class TDS_API UTDSStateEffectOnceExecution : public UTDSStateEffects
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Once Exec")
	float Power = 20.f;

public:
	virtual bool InitObject(AActor* Actor, FName HittedBone) override;
	virtual void DestroyObject() override;
	virtual void ExecuteOnce();
};

UCLASS()
class TDS_API UTDSStateEffectTimerExecution : public UTDSStateEffects
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Timer Exec")
	float Power = 20.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Timer Exec")
	float Timer = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Timer Exec")
	float TimerRate = 1.f;

	FTimerHandle ExecutionTimer;
	FTimerHandle EffectTimer;

public:
	
	virtual bool InitObject(AActor* Actor, FName HittedBone) override;
	virtual void DestroyObject() override;
	virtual void Execute();
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "ProjectileDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "TDSStateEffects.h"
#include "Net/UnrealNetwork.h"
#include "../InventorySystem.h"
#include "../../Character/TDSCharacter.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Weapon"));
	SkeletalMesh->SetGenerateOverlapEvents(false);
	SkeletalMesh->SetCollisionProfileName(TEXT("NoCollison"));
	SkeletalMesh->SetupAttachment(RootComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Statick Weapon"));
	StaticMesh->SetGenerateOverlapEvents(false);
	StaticMesh->SetCollisionProfileName(TEXT("NoCollison"));
	StaticMesh->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	SetReplicates(true);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(HasAuthority()){

		FireTick(DeltaTime);
		ReloadTick(DeltaTime);
		DispersionTick(DeltaTime);
		ClipDropTick(DeltaTime);
		ShellDropTick(DeltaTime);
		
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (GetWeaponClipSize()) {
		if (WeaponFiring) {
			if (FireTimer <= 0.0f) {
				if (!WeaponReloading) {
					Fire();
				}
			}
			else {
				FireTimer -= DeltaTime;
			}
		}
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading) {
		if (ReloadTimer < 0.0f) {
			FinishReload();
		}
		else {
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading) {
		if (!WeaponFiring) {
			CurrentDispersion -= CurrentDispersionReduction;
		}else
			CurrentDispersion += CurrentDispersionReduction;

		if (CurrentDispersion < CurrentDispersionMin) {
			CurrentDispersion = CurrentDispersionMin;
		}
		else {
			if (CurrentDispersion > CurrentDispersionMax) {
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (bDropBulletShell) {
		if (DropShellTimer < 0.f) {
			bDropBulletShell = false;
			
			InitDropMesh_OnServer(WeaponSettings.BulletShellDrop.DropMesh,
				WeaponSettings.BulletShellDrop.DropMeshOffset,
				WeaponSettings.BulletShellDrop.DropMeshImpulseDir,
				WeaponSettings.BulletShellDrop.DropMeshLifeTime,
				WeaponSettings.BulletShellDrop.ImpulseRandomDispersion,
				WeaponSettings.BulletShellDrop.PowerImpulse,
				WeaponSettings.BulletShellDrop.CustomMass);
		}
		else {
			DropShellTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (bDropClip) {
		if (DropClipTimer < 0.f) {
			bDropClip = false;

			SkeletalMesh->HideBoneByName(FName("Clip_Bone"), EPhysBodyOp::PBO_None);

			InitDropMesh_OnServer(WeaponSettings.ClipDrop.DropMesh,
				WeaponSettings.ClipDrop.DropMeshOffset,
				WeaponSettings.ClipDrop.DropMeshImpulseDir,
				WeaponSettings.ClipDrop.DropMeshLifeTime,
				WeaponSettings.ClipDrop.ImpulseRandomDispersion,
				WeaponSettings.ClipDrop.PowerImpulse,
				WeaponSettings.ClipDrop.CustomMass);
		}
		else {
			DropClipTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::InitDropMesh_OnServer_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDir, float MeshLifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)
{
	if (DropMesh) {
		
		FTransform Transform;

		FVector LocalDir = this->GetActorForwardVector() * Offset.GetLocation().X 
			+ this->GetActorRightVector() * Offset.GetLocation().Y 
			+ this->GetActorUpVector() * Offset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((GetActorRotation() + Offset.Rotator()).Quaternion());

		ShellDropFire_Multicast(DropMesh, Transform, DropImpulseDir, MeshLifeTime, ImpulseRandomDispersion, PowerImpulse, CustomMass, LocalDir);
	}
}

float AWeaponDefault::GetWeaponReloadTime()
{
	return WeaponSettings.ReloadTime;
}

float AWeaponDefault::GetWeaponShootingZCoord()
{
	if (ShootLocation) {
		return ShootLocation->GetComponentLocation().Z;
	}
	return 0.0f;
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMesh && !SkeletalMesh->SkeletalMesh) {
		SkeletalMesh->DestroyComponent();
	}

	if (StaticMesh && !StaticMesh->GetStaticMesh()) {
		StaticMesh->DestroyComponent();
	}

	UpdateStateWeapon_OnServer(EMovementState::RunState);
}

void AWeaponDefault::SetWeaponFireState_OnServer_Implementation(bool bIsFire)
{
	if (CheckWeaponCanFire()) {
		WeaponFiring = bIsFire;
	}
	else {
		WeaponFiring = false;
		FireTimer = 0.01f;
	}

}

bool AWeaponDefault::CheckWeaponCanFire() const
{
	return !BlockFire;
}

bool AWeaponDefault::CanWeaponReload()
{
	bool result = true;
	if (GetOwner()) {
		UInventorySystem* MyInv = Cast<UInventorySystem>(GetOwner()->GetComponentByClass(UInventorySystem::StaticClass()));
		if (MyInv) {
			int32 AvailableAmmo;
			if (!MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmo)){
				result = false;

				MyInv->WeaponHaveNoAmmoEvent_Multicast(MyInv->GetWeaponIndexSlotByName(CurrentWeaponName));
			}
			else
			{
				MyInv->WeaponHaveAmmoEvent_Multicast(MyInv->GetWeaponIndexSlotByName(CurrentWeaponName));
			}
		}
	}
	return result;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSettings.ProjectileSettings;
}

void AWeaponDefault::Fire()
{
	//OnServer - WeaponFireBool

	UAnimMontage* AnimToPlay = nullptr;

	if (WeaponAiming) {
		AnimToPlay = WeaponSettings.ShootAnims.AnimCharFireAim;
	}
	else {
		AnimToPlay = WeaponSettings.ShootAnims.AnimCharFire;
	}

	if(WeaponSettings.ShootAnims.AnimWeaponFire)
		AnimWeaponStart_Multicast(WeaponSettings.ShootAnims.AnimWeaponFire, 1.f);

	if (WeaponSettings.BulletShellDrop.DropMesh) {
		if (WeaponSettings.BulletShellDrop.DropMeshTime < 0.f) {

			InitDropMesh_OnServer(WeaponSettings.BulletShellDrop.DropMesh, 
				WeaponSettings.BulletShellDrop.DropMeshOffset, 
				WeaponSettings.BulletShellDrop.DropMeshImpulseDir, 
				WeaponSettings.BulletShellDrop.DropMeshLifeTime,
				WeaponSettings.BulletShellDrop.ImpulseRandomDispersion, 
				WeaponSettings.BulletShellDrop.PowerImpulse,
				WeaponSettings.BulletShellDrop.CustomMass);
		}
		else {
			bDropBulletShell = true;
			DropShellTimer = WeaponSettings.BulletShellDrop.DropMeshTime;
		}
	}

	int8 NumberProjectile = GetNumberProjectileByShot();
	FireTimer = WeaponSettings.RateOfFire;
	WeaponInfo.ClipSize -= 1;
	ChangeDispersionByShot();

	FXWeaponFire_Multicast(WeaponSettings.EffectFireWeapon, WeaponSettings.SoundFireWeapon);
	OnWeaponFireStart.Broadcast(AnimToPlay);
	
	if (ShootLocation) {

		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		
		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++) {

			EndLocation = GetFireEndLocation();

			if (ProjectileInfo.Projectile) {

				FVector Dir = EndLocation - SpawnLocation;

				Dir.Normalize();

				FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
				SpawnRotation = myMatrix.Rotator();

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));

				if (myProjectile) {
					myProjectile->InitProjectile(WeaponSettings.ProjectileSettings);
				}
			}
			else {
				
				//TODO: Multicast trace FX
				FHitResult ResHit;
				TArray<AActor*> Actors;
				
				EDrawDebugTrace::Type DebugTrace;
				if (ShowDebug) {
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector()*WeaponSettings.DistanceTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else {
					DebugTrace = EDrawDebugTrace::None;
				}

				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSettings.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, ResHit, 
					true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				if (ResHit.GetActor() && ResHit.PhysMaterial.IsValid())
				{

					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(ResHit);

					if (ProjectileInfo.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = ProjectileInfo.HitDecals[mySurfacetype];

						if (myMaterial)
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Cast<UPrimitiveComponent>(ResHit.Component), NAME_None, ResHit.ImpactPoint, ResHit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}
					if (ProjectileInfo.HitFX.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = ProjectileInfo.HitFX[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(ResHit.ImpactNormal.Rotation(), ResHit.ImpactPoint, FVector(1.0f)));
						}
						else UE_LOG(LogTemp, Warning, TEXT("Hit Perticle Null"));
					}

					if (ProjectileInfo.HitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileInfo.HitSound, ResHit.ImpactPoint);
					}
					else UE_LOG(LogTemp, Warning, TEXT("Hit Sound Null"));


					UTypes::AddEffectBySurfaceType(ResHit.GetActor(), ProjectileInfo.Effect, mySurfacetype, ResHit.BoneName);

					UGameplayStatics::ApplyDamage(ResHit.GetActor(), WeaponSettings.WeaponDamage, GetInstigatorController(), this, NULL);

				}
			}
		}
	}

	if (!WeaponReloading && GetWeaponClipSize() <= 0)
		if(CanWeaponReload())
			InitReload();
}

void AWeaponDefault::UpdateStateWeapon_OnServer_Implementation(EMovementState NewMovementState)
{
	BlockFire = false;
	switch (NewMovementState) {
	case EMovementState::AimState:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimState_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimState_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimState_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimState_DispersionReduction;
		break;
	case EMovementState::AimWalkState:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.AimWalkState_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.AimWalkState_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.AimWalkState_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.AimWalkState_DispersionReduction;
		break;
	case EMovementState::WalkState:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.WalkState_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.WalkState_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.WalkState_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.WalkState_DispersionReduction;
		break;
	case EMovementState::RunState:
		CurrentDispersionMax = WeaponSettings.DispersionWeapon.RunState_DispersionAimMax;
		CurrentDispersionMin = WeaponSettings.DispersionWeapon.RunState_DispersionAimMin;
		CurrentDispersionRecoil = WeaponSettings.DispersionWeapon.RunState_DispersionAimRecoil;
		CurrentDispersionReduction = WeaponSettings.DispersionWeapon.RunState_DispersionReduction;
		break;
	case EMovementState::SprintRunState:
		BlockFire = true;
		SetWeaponFireState_OnServer(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic) {
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if(ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSettings.DistanceTrace, GetCurrentDispersion()*PI/180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSettings.DistanceTrace, GetCurrentDispersion() * PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 1.f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 1.f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Yellow, false, 5.f, (uint8)'\000', 1.f);

	}

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSettings.NumberProjectileByShot;
}

EWeaponType AWeaponDefault::GetWeaponType() const
{
	return WeaponSettings.WeaponType;
}

int32 AWeaponDefault::GetWeaponClipSize()
{
	return WeaponInfo.ClipSize;
}

float AWeaponDefault::WeaponShootDistance()
{
	const float Distance = WeaponSettings.ProjectileSettings.ProjectileInitSpeed;;
	return Distance;
}

//FIX shoots with large delay
void AWeaponDefault::FXWeaponFire_Multicast_Implementation(UParticleSystem* FXFire, USoundBase* SoundFire)
{
	if (FXFire) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), FXFire, ShootLocation->GetComponentTransform());
	}
	if (SoundFire) {
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundFire, ShootLocation->GetComponentLocation());
	}
}

void AWeaponDefault::ShellDropFire_Multicast_Implementation(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDir, float MeshLifeTime, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass, FVector LocalDir)
{
	AStaticMeshActor* MyActor = nullptr;
	FActorSpawnParameters Params;
	Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Params.Owner = this;

	MyActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Offset, Params);

	if (MyActor && MyActor->GetStaticMeshComponent()) {
		MyActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		MyActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

		MyActor->SetActorTickEnabled(false);
		MyActor->InitialLifeSpan = MeshLifeTime;

		MyActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		MyActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		MyActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
		MyActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);
	}

	if (CustomMass > 0.f) {
		MyActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
	}

	if (!DropImpulseDir.IsNearlyZero()) {
		FVector FinalDir;
		LocalDir = LocalDir + (DropImpulseDir * 1000.f);

		if (!FMath::IsNearlyZero(ImpulseRandomDispersion)) {
			FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion);
		}

		FinalDir.GetSafeNormal(0.0001f);
		
		MyActor->GetStaticMeshComponent()->AddImpulse(FinalDir * PowerImpulse);
	}
}

void AWeaponDefault::AnimWeaponStart_Multicast_Implementation(UAnimMontage* Anim, float PlayRate)
{
	if (Anim
		&& SkeletalMesh
		&& SkeletalMesh->GetAnimInstance()) {

		SkeletalMesh->GetAnimInstance()->Montage_Play(Anim, PlayRate);

	}
}

void AWeaponDefault::UpdateWeaponByCharacterMovementState_OnServer_Implementation(FVector NewShootEndLocation, bool NewShouldReduceDispersion)
{
	ShootEndLocation = NewShootEndLocation;
	ShouldReduceDispersion = NewShouldReduceDispersion;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSettings.ReloadTime;
	
	UAnimMontage* AnimToPlay = nullptr;

	if (WeaponAiming) {
		AnimToPlay = WeaponSettings.ShootAnims.AnimCharReloadAim;
	}
	else {
		AnimToPlay = WeaponSettings.ShootAnims.AnimCharReload;
	}

	UAnimMontage* WeaponAnim = nullptr;

	if (WeaponAiming) {
		WeaponAnim = WeaponSettings.ShootAnims.AnimWeaponReloadAim;
	}
	else {
		WeaponAnim = WeaponSettings.ShootAnims.AnimWeaponReload;
	}
	
	OnWeaponReloadStart.Broadcast(AnimToPlay, WeaponSettings.ReloadTime);

	if (WeaponAnim
		&& SkeletalMesh
		&& SkeletalMesh->GetAnimInstance()) {

		float PlayRate = 1.f / (WeaponSettings.ReloadTime / WeaponAnim->GetPlayLength());
		SkeletalMesh->GetAnimInstance()->Montage_Play(WeaponAnim, PlayRate);
		AnimWeaponStart_Multicast(WeaponAnim, PlayRate);
	}

	if (WeaponSettings.ClipDrop.DropMesh) {
		bDropClip = true;
		DropClipTimer = WeaponSettings.ClipDrop.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	int32 AvailableAmmoInInventory = GetAvailableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int32 NeedToReload = WeaponSettings.MaxClipSize - WeaponInfo.ClipSize;

	SkeletalMesh->UnHideBoneByName(FName("Clip_Bone"));

	if (NeedToReload > AvailableAmmoInInventory) {
		WeaponInfo.ClipSize += AvailableAmmoInInventory;
		AmmoNeedTakeFromInv = AvailableAmmoInInventory;
	}
	else {
		WeaponInfo.ClipSize += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMesh && SkeletalMesh->GetAnimInstance())
		SkeletalMesh->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	bDropClip = false;
}

int32 AWeaponDefault::GetAvailableAmmoForReload()
{
	int32 AvailableAmmo = WeaponSettings.MaxClipSize;

	if (GetOwner()) {
		UInventorySystem* MyInv = Cast<UInventorySystem>(GetOwner()->GetComponentByClass(UInventorySystem::StaticClass()));
		if (MyInv) {
			if (!MyInv->CheckAmmoForWeapon(WeaponSettings.WeaponType, AvailableAmmo)) {

			}
		}
	}
	return AvailableAmmo;
}

void AWeaponDefault::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AWeaponDefault, WeaponInfo);
	DOREPLIFETIME(AWeaponDefault, ShootEndLocation);
	DOREPLIFETIME(AWeaponDefault, WeaponReloading);
}
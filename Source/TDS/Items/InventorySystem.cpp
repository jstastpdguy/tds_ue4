// Fill out your copyright notice in the Description page of Project Settings.


#include "InventorySystem.h"

#include "GeneratedCodeHelpers.h"
#include "../Interface/TDS_I_GameActors.h"
#include "../Game/TDSGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UInventorySystem::UInventorySystem()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UInventorySystem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void UInventorySystem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//reccurent function which switch weapon to index? if it's valid, 
//otherwise call itself again, while didn't find succsessfull slot
bool UInventorySystem::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex)
{
	bool bIsSuccess = false;
	int32 NewIndex;
	
	NewIndex = ChangeToIndex % WeaponSlots.Num();

	if (NewIndex < 0) {
		NewIndex = WeaponSlots.Num() + NewIndex;
	}

	if (NewIndex == OldIndex)
		return false;

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;

	if (WeaponSlots.IsValidIndex(NewIndex)) {
		if (!WeaponSlots[NewIndex].NameItem.IsNone()) {
			if (WeaponSlots[NewIndex].AdditionalInfo.ClipSize > 0) {
				bIsSuccess = true;
			}
			else {
				UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI) {
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[NewIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind) {
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Count > 0) {
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess) {
				NewIdWeapon = WeaponSlots[NewIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[NewIndex].AdditionalInfo;
			}
		}
	}

	if (bIsSuccess) {
		SwitchWeaponEvent_OnServer(NewIdWeapon, NewAdditionalInfo, NewIndex);
		UpdateWidgetByChangeWeaponEvent_Multicast(NewIndex);
	}
	//if next weapon has no ammo find next available in forward direction if E was pressed, or backward, if - Q
	else {
		//forward
		if (ChangeToIndex > OldIndex) {
			if (SwitchWeaponToIndex(ChangeToIndex + 1, OldIndex)) {
				return true;
			}
		}
		//backward
		else {
			if (SwitchWeaponToIndex(ChangeToIndex - 1, OldIndex)) {
				return true;
			}
		}
	}
	//if any weapon has no ammo return false
	return bIsSuccess;
}

FAdditionalWeaponInfo UInventorySystem::GetAdditionalWeaponInfo(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind) {
			if (i == IndexWeapon) {
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("FAdditionalWeaponInfo UInventorySystem::GetAdditionalWeaponInfo - Didn't found weapon by index %d"), IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("FAdditionalWeaponInfo UInventorySystem::GetAdditionalWeaponInfo - Not correct index weapon - %d"), IndexWeapon);
	}
	return result;
}

int32 UInventorySystem::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind) {
		if (WeaponSlots[i].NameItem == IdWeaponName) {
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

void UInventorySystem::GetWeaponNameByIndex(FName& Name, int32 WeaponSlot)
{
	if (WeaponSlots.IsValidIndex(WeaponSlot)) {
		Name = WeaponSlots[WeaponSlot].NameItem;
	}
}

void UInventorySystem::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon)) {
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind){
			if (i == IndexWeapon) {
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind) {
			UE_LOG(LogTemp, Warning, TEXT("UInventorySystem::SetAdditionalInfoWeapon - Didn't found weapon by index %d"), 
				IndexWeapon);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UInventorySystem::SetAdditionalInfoWeapon - Not correct index weapon - %d"), 
			IndexWeapon);
	}
}

void UInventorySystem::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 ChangeAmmoValue)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind) {
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			AmmoSlots[i].Count += ChangeAmmoValue;
			if (AmmoSlots[i].Count > AmmoSlots[i].MaxCount)
				AmmoSlots[i].Count = AmmoSlots[i].MaxCount;

			if (AmmoSlots[i].Count <= 0)
				AmmoTypeEmptyEvent_Multicast(TypeWeapon);
			else
				AmmoTypeAvailableEvent_Multicast(TypeWeapon);

			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Count);

			bIsFind = true;
		}
		i++;
	}
}

bool UInventorySystem::CheckAmmoForWeapon(EWeaponType TypeWeapon, int32 &AvailableAmmo)
{
	AvailableAmmo = 0;
	int8 i = 0;
	while (i < AmmoSlots.Num()) {
		if (AmmoSlots[i].WeaponType == TypeWeapon) {
			AvailableAmmo = AmmoSlots[i].Count;
			if (AmmoSlots[i].Count > 0) {
				return true;
			}
		}
		i++;
	}

	AmmoTypeEmptyEvent_Multicast(TypeWeapon);
	return false;
}

bool UInventorySystem::CheckCanTakeWeapon(int32 &FreeSlotIndex)
{
	bool bIsFree = false;
	for (auto i = 0; i < WeaponSlots.Num(); i++) {
		if (WeaponSlots[i].NameItem.IsNone()) {
			FreeSlotIndex = i;
			return true;
		}
	}
	return bIsFree;
}

bool UInventorySystem::CheckCanTakeAmmo(EWeaponType AmmoType)
{
	bool result = false;
	for (auto i : AmmoSlots) {
		if (i.WeaponType == AmmoType && i.Count < i.MaxCount) {
			result = true;
			return result;
		}
	}
	return result;
}

bool UInventorySystem::SwitchItemToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeapon, 
	FDropItem &DropItemInfo)
{
	bool result = false;
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemFromInventory(IndexSlot, DropItemInfo)) {
		WeaponSlots[IndexSlot] = NewWeapon;
		SwitchWeaponEvent_OnServer(NewWeapon.NameItem, NewWeapon.AdditionalInfo, IndexSlot);
		UpdateWidgetByChangeWeaponEvent_Multicast(IndexSlot);
		UpdateWeaponSLotEvent_Multicast(IndexSlot, NewWeapon);
		result = true;
	}
	return result;
}

void UInventorySystem::SaveItemToInventory_OnServer_Implementation(FWeaponSlot NewWeapon, AActor* PickUpActor)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot)) {
		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			WeaponSlots[IndexSlot] = NewWeapon;
			UpdateWeaponSLotEvent_Multicast(IndexSlot, NewWeapon);

			if(PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}
	}
}

//sets DropItem variable
bool UInventorySystem::GetDropItemFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	
	if (myGI) {
		result = myGI->DropItemInfoByName(WeaponSlots[IndexSlot].NameItem, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot)) {
			DropItemInfo.WeaponInfo.NameItem = WeaponSlots[IndexSlot].NameItem;
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}

	return result;
}

void UInventorySystem::DropWeaponByIndex_OnServer_Implementation(int32 SlotIndex)
{
	FDropItem DropItemInfo;
	FWeaponSlot EmptyWeaponSlot;
	bool bCanDrop = false;

	if (WeaponSlots.IsValidIndex(SlotIndex) 
		&& WeaponSlots[SlotIndex].NameItem != FName() 
		&& WeaponSlots[SlotIndex].NameItem != FName("Pistol_Def"))
		bCanDrop = true;

	if (bCanDrop && GetDropItemFromInventory(SlotIndex, DropItemInfo))
		SwitchWeaponToIndex(SlotIndex + 1, SlotIndex);

	WeaponSlots[SlotIndex] = EmptyWeaponSlot;

	if (GetOwner()->GetClass()->ImplementsInterface(UTDS_I_GameActors::StaticClass()))
		ITDS_I_GameActors::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);

	UpdateWeaponSLotEvent_Multicast(SlotIndex, EmptyWeaponSlot);
}

TArray<FWeaponSlot> UInventorySystem::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UInventorySystem::GetAmmoSlots()
{
	return AmmoSlots;
}

void UInventorySystem::UpdateWidgetByChangeWeaponEvent_Multicast_Implementation(int32 NewWeaponIndex)
{
	OnChangeWeaponUpdateWidget.Broadcast(NewWeaponIndex);
}

void UInventorySystem::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(int32 SlotIndex,
																				FAdditionalWeaponInfo NewWeaponInfo)
{
	OnAdditionalInfoChange.Broadcast(SlotIndex, NewWeaponInfo);
}

void UInventorySystem::WeaponHaveAmmoEvent_Multicast_Implementation(int32 SlotIndex)
{
	OnWeaponHaveAmmo.Broadcast(SlotIndex);
}

void UInventorySystem::WeaponHaveNoAmmoEvent_Multicast_Implementation(int32 SlotIndex)
{
	OnWeaponHaveNoAmmo.Broadcast(SlotIndex);
}

void UInventorySystem::UpdateWeaponSLotEvent_Multicast_Implementation(int32 SlotIndex, FWeaponSlot NewWeapon)
{
	OnUpdateWeaponSlots.Broadcast(SlotIndex, NewWeapon);
}

void UInventorySystem::AmmoTypeAvailableEvent_Multicast_Implementation(EWeaponType WeaponType)
{
	OnAmmoTypeAvailable.Broadcast(WeaponType);
}

void UInventorySystem::AmmoTypeEmptyEvent_Multicast_Implementation(EWeaponType WeaponType
)
{
	OnAmmoTypeEmpty.Broadcast(WeaponType);
}

void UInventorySystem::SwitchWeaponEvent_OnServer_Implementation(FName WeaponName, FAdditionalWeaponInfo WeaponInfo,
																  int32 NewWeaponIndex)
{
	OnSwitchWeapon.Broadcast(WeaponName, WeaponInfo, NewWeaponIndex);
}

void UInventorySystem::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, 
	const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0)) {
		if (!WeaponSlots[0].NameItem.IsNone()) {
			SwitchWeaponEvent_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			UpdateWidgetByChangeWeaponEvent_Multicast(0);
		}
	}
}

void UInventorySystem::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Count)
{
	OnAmmoChange.Broadcast(TypeWeapon, Count);
}

void UInventorySystem::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UInventorySystem, WeaponSlots);
	DOREPLIFETIME(UInventorySystem, AmmoSlots);
	
}
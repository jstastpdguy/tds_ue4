// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSEnvironmentStruct.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS/Items/Weapon/TDSStateEffects.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATDSEnvironmentStruct::ATDSEnvironmentStruct()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATDSEnvironmentStruct::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ATDSEnvironmentStruct::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

EPhysicalSurface ATDSEnvironmentStruct::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh) {
		UMaterialInterface* myMat = myMesh->GetMaterial(0);
		if (myMat) {
			Result = myMat->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}

TArray<UTDSStateEffects*> ATDSEnvironmentStruct::GetAllCurrentEffects()
{
	return Effects;
}

void ATDSEnvironmentStruct::RemoveEffect(UTDSStateEffects* ToRemove)
{
	Effects.Remove(ToRemove);
	
	if(!ToRemove->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToRemove, false);
		RemoveEffectRep = ToRemove;
	}
}

void ATDSEnvironmentStruct::AddEffect(UTDSStateEffects* ToAdd)
{
	Effects.Add(ToAdd);
	
	if(!ToAdd->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToAdd, true);
		AddEffectRep = ToAdd;
	}else
	{
		if(ToAdd->GetParticleEffect())
		{
			ExecuteEffectAdded_OnServer(const_cast<UParticleSystem*>(ToAdd->GetParticleEffect()));
		}
	}
}

void ATDSEnvironmentStruct::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDSEnvironmentStruct::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, EffectOffset, NAME_None);
}

void ATDSEnvironmentStruct::EffectAdd_OnRep()
{
	if(AddEffectRep)
	{
		UTypes::SwitchEffect(this, AddEffectRep, true);
	}
}

void ATDSEnvironmentStruct::EffectRemove_OnRep()
{
	if(RemoveEffectRep)
	{
		UTypes::SwitchEffect(this, RemoveEffectRep, false);
	}
}

bool ATDSEnvironmentStruct::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for(int32 i = 0; i < Effects.Num(); i++)
	{
		if(Effects[i]) { Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); }
	}
	return Wrote;
}

void ATDSEnvironmentStruct::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(ATDSEnvironmentStruct, AddEffectRep);
	DOREPLIFETIME(ATDSEnvironmentStruct, RemoveEffectRep);
	DOREPLIFETIME(ATDSEnvironmentStruct, Effects)
}
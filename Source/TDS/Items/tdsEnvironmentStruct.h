// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Interface/TDS_I_GameActors.h"
#include "Weapon/TDSStateEffects.h"
#include "TDSEnvironmentStruct.generated.h"

UCLASS()
class TDS_API ATDSEnvironmentStruct : public AActor, public ITDS_I_GameActors
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDSEnvironmentStruct();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	
public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effects")
	FVector EffectOffset = FVector::ZeroVector;
	UPROPERTY(Replicated)
	TArray<UTDSStateEffects*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDSStateEffects* AddEffectRep = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDSStateEffects* RemoveEffectRep = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//interface
	virtual EPhysicalSurface GetSurfaceType() override;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual TArray<UTDSStateEffects*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDSStateEffects* ToRemove) override;
	virtual void AddEffect(UTDSStateEffects* ToAdd) override;
	virtual USceneComponent* GetSceneComponentToAttachEffect() override {	return RootComponent;	}
	virtual TArray<UParticleSystemComponent*>* GetAllParticleComponents() override { return &ParticleSystemEffects; };
	virtual FVector GetEffectOffset() override { return EffectOffset; };

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();

};
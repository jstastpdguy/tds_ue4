// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "../FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "../Items/Weapon/WeaponDefault.h"
#include "TDSGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "WeaponSettings")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DropItems")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool DropItemInfoByName(FName WeaponItem, FDropItem& OutInfo);
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCameraShake.h"

UMyCameraShake::UMyCameraShake() {
    OscillationDuration = 0.1f;
    OscillationBlendInTime = 0.1f;
    OscillationBlendOutTime = 0.2f;

    RotOscillation.Pitch.Amplitude = 20.0f;
    RotOscillation.Pitch.Frequency = 60.0f;

    RotOscillation.Yaw.Amplitude = 20.0f;
    RotOscillation.Yaw.Frequency = 60.0f;

    LocOscillation.X.Amplitude = 10.0f;
    LocOscillation.X.Frequency = 1.0f;
    LocOscillation.X.InitialOffset = EOO_OffsetZero;

    LocOscillation.Z.Amplitude = 10.0f;
    LocOscillation.Z.Frequency = 1.0f;
    LocOscillation.Z.InitialOffset = EOO_OffsetZero;
}
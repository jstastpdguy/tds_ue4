// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo)
{
	FWeaponInfo* WeaponInfoRow;

	if (WeaponInfoTable) {
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);
		if (WeaponInfoRow) {
			OutInfo = *WeaponInfoRow;
			return true;
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
	}
	return false;
}

bool UTDSGameInstance::DropItemInfoByName(FName WeaponItem, FDropItem& OutInfo)
{
	if (DropItemInfoTable) {
		FDropItem* DropItemRow;
		TArray<FName> RowNames = DropItemInfoTable->GetRowNames();
		
		for (auto i : RowNames) {
			DropItemRow = DropItemInfoTable->FindRow<FDropItem>(i, "");
			if (DropItemRow->WeaponInfo.NameItem == WeaponItem) {
				OutInfo = *DropItemRow;
				return true;
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - WeaponTable - NULL"));
	}
	return false;
}

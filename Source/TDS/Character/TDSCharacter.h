// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "../Interface/TDS_I_GameActors.h"
#include "TDSCharacter.generated.h"

class AWeaponDefault;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter, public ITDS_I_GameActors
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void OnConstruction(const FTransform& Transform) override;

	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	//input axises
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float MouseAxis = 0.0f;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Inventory, meta = (AllowPrivateAccess = "true"))
	class UInventorySystem* InventoryComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (AllowPrivateAccess = "true"))
	class UCharacterHealthComponent* HealthComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	bool Flow = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float HeightMin = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float HeightMax = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float HeightChangeCoeff = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float SmoothCoef = 5.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	FRotator CameraRotator = FRotator(0.0f, -90.0f, 0.0f);

	//timers
	FTimerHandle CameraSlideTimer;
	FTimerHandle TimerHandle_RagdollActivating;
	FTimerHandle PickUpWeaponTimer;
	FTimerHandle DropWeaponTimer;

	TArray<UParticleSystemComponent*> ParticleSystemEffects;

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float DropWeaponDelayTime = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Time")
	float PickUpWeaponTime = 1.f;

	UFUNCTION()
	void StartDropWeaponTimer(FKey KeyPressed);
	UFUNCTION()
	void ClearDropWeaponTimer();
	
	UFUNCTION(BlueprintNativeEvent)
	void StartDropWeaponTimer_BP(FTimerHandle Timer, float Time, FKey KeyPressed);
	UFUNCTION(BlueprintNativeEvent)
	void ClearDropWeaponTimer_BP();

	//Weapon
	UPROPERTY(Replicated)
	AWeaponDefault* CurrentWeapon = nullptr;
	//inventory index
	UPROPERTY(Replicated, BlueprintReadOnly, EditDefaultsOnly)
	int32 CurrentIndexWeapon = 0;

	//Movement
	UPROPERTY(Replicated)
	EMovementState MovementState = EMovementState::RunState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	//Camera smooth slide
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
	float TickSlideValue = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
	float TickCounter = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
	float TimerTickValue = 0.01f;

	//slide flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
	bool SlideUp = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Slide")
	bool SlideDone = true;

	//Crosshair
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Crosshair")
	UMaterialInterface* CrosshairMat = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Crosshair")
	FVector CrosshairSize = FVector(20.0f, 40.0f, 40.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
	TSubclassOf<UTDSStateEffects> AbilityEffect;

	UPROPERTY(Replicated)
	TArray<UTDSStateEffects*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDSStateEffects* AddEffectRep = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDSStateEffects* RemoveEffectRep = nullptr;
	
	//Character State flags
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bSprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bWalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool bAimEnabled = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	TArray<UAnimMontage*> DeadAnims;

	//tick funcs
	UFUNCTION()
	void MovementTick(float DeltaTime);

	//inputs
	UFUNCTION()
	void InputAxisX(float value);

	UFUNCTION()
	void InputAxisY(float value);

	UFUNCTION()
	void InputMouseAxis(float value);

	UFUNCTION()
	void InputAttackPressed();
	UFUNCTION()
	void InputAttackReleased();

	//switch weapon depending on key was pressed
	UFUNCTION(Server, Reliable)
	void TrySwitchWeapon_OnServer(FKey Direction);

	//boolean flags
	UFUNCTION()
	void WalkEnable();
	UFUNCTION()
	void WalkDisable();

	UFUNCTION()
	void SpeedRunEnable();
	UFUNCTION()
	void SpeedRunDisable();

	UFUNCTION()
	void AimEnable();
	UFUNCTION()
	void AimDisable();

	//weapon
	UFUNCTION(BlueprintCallable, BlueprintPure)
	AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName WeaponName, FAdditionalWeaponInfo WeaponInfo, int32 NewCurrentWeaponIndex);	
	UFUNCTION(BlueprintCallable)
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
	void TryReloadWeapon();
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void TryUseAbility_OnServer();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	int32 GetCurrentWeaponIndex();

	//camera
	UFUNCTION(BlueprintCallable)
	void CameraShake();
	UFUNCTION(BlueprintCallable)
	void CameraSlide(float AxisValue);
	UFUNCTION(BlueprintCallable)
	void CameraSlideSmooth();

	//movement states
	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();
	UFUNCTION(BlueprintCallable, BlueprintPure)
	EMovementState GetMovementState();

	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim, float ReloadTime);
	UFUNCTION()
	void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);

	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim, float ReloadTime);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP(bool bIsSuccess, int32 AmmoSafe);

	//interface
	virtual EPhysicalSurface GetSurfaceType() override;
	UFUNCTION(BlueprintCallable, BlueprintPure)
	virtual TArray<UTDSStateEffects*> GetAllCurrentEffects() override;
	virtual void RemoveEffect(UTDSStateEffects* ToRemove) override;
	virtual void AddEffect(UTDSStateEffects* ToAdd) override;
	virtual USceneComponent* GetSceneComponentToAttachEffect() override;
	virtual TArray<UParticleSystemComponent*>* GetAllParticleComponents() override { return &ParticleSystemEffects; };

	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool GetIsAlive();

	UFUNCTION()
	void CharacterDead();
	UFUNCTION(BlueprintNativeEvent)
	void CharacterDead_BP();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagdoll_Multicast();

	bool CanDropWeapon();
	void DropCurrentWeapon();
	void StartPickUpTimer(FKey KeyPressed);

	//Multiplayer
	UFUNCTION(Server, Unreliable)
	void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
	void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
	void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
	void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
	void TryReloadWeapon_OnServer();
	UFUNCTION(NetMulticast, Unreliable)
	void PlayAnim_Multicast(UAnimMontage* MontageToPlay);

	UFUNCTION(NetMulticast, Unreliable)
	void SetWeaponTypeToAnimInst_Multicast(EWeaponType NewType);
	UFUNCTION(BlueprintNativeEvent)
	void SetWeaponTypeAnimInst_BP(EWeaponType NewType);
	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);
	UFUNCTION()
	void EffectRemove_OnRep();

protected:
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
};
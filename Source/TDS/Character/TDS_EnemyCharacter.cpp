// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnemyCharacter.h"
#include "TDSCharacter.h"
#include "Engine/ActorChannel.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATDS_EnemyCharacter::ATDS_EnemyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ATDS_EnemyCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for(int32 i = 0; i < Effects.Num(); i++)
	{
		if(Effects[i]) { Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); }
	}
	return Wrote;
}

// Called every frame
void ATDS_EnemyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDS_EnemyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATDS_EnemyCharacter::RemoveEffect(UTDSStateEffects* ToRemove)
{
	Effects.Remove(ToRemove);
	
	if(!ToRemove->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToRemove, false);
		RemoveEffectRep = ToRemove;
	}
}

void ATDS_EnemyCharacter::AddEffect(UTDSStateEffects* ToAdd)
{
	Effects.Add(ToAdd);

	if(!ToAdd->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToAdd, true);
		AddEffectRep = ToAdd;
	}else
	{
		if(ToAdd->GetParticleEffect())
		{
			ExecuteEffectAdded_OnServer(const_cast<UParticleSystem*>(ToAdd->GetParticleEffect()));
		}
	}
}

void ATDS_EnemyCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDS_EnemyCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("spine_01"));
}

void ATDS_EnemyCharacter::EffectAdd_OnRep()
{
	if(AddEffectRep)
	{
		UTypes::SwitchEffect(this, AddEffectRep, true);
	}
}

void ATDS_EnemyCharacter::EffectRemove_OnRep()
{
	if(RemoveEffectRep)
	{
		UTypes::SwitchEffect(this, RemoveEffectRep, false);
	}
}

void ATDS_EnemyCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDS_EnemyCharacter, Effects);
	DOREPLIFETIME(ATDS_EnemyCharacter, AddEffectRep);
	DOREPLIFETIME(ATDS_EnemyCharacter, RemoveEffectRep);
}
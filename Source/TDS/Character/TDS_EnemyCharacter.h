// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/Interface/TDS_I_GameActors.h"
#include "TDS_EnemyCharacter.generated.h"

UCLASS()
class TDS_API ATDS_EnemyCharacter : public ACharacter, public ITDS_I_GameActors
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDS_EnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(Replicated)
	TArray<UTDSStateEffects*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
	UTDSStateEffects* AddEffectRep = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
	UTDSStateEffects* RemoveEffectRep = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//interface
	virtual void RemoveEffect(UTDSStateEffects* ToRemove) override;
	virtual void AddEffect(UTDSStateEffects* ToAdd) override;
	virtual USceneComponent* GetSceneComponentToAttachEffect() override {	return GetMesh();	}
	virtual TArray<UParticleSystemComponent*>* GetAllParticleComponents() override { return &ParticleSystemEffects; };

	UFUNCTION(Server, Reliable)
	void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
	void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
	void EffectAdd_OnRep();
	UFUNCTION()
	void EffectRemove_OnRep();
};

// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "../Game/TDSGameInstance.h"
#include "../Items/Weapon/WeaponDefault.h"
#include "Engine/World.h"
#include "../Items/InventorySystem.h"
#include "../Character/CharacterHealthComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "../Items/Weapon/ProjectileDefault.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "TDS/Items/Weapon/TDSStateEffects.h"
#include "Engine/ActorChannel.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UInventorySystem>(TEXT("Inventory"));
	HealthComponent = CreateDefaultSubobject<UCharacterHealthComponent>(TEXT("Health"));

	if (HealthComponent) {
		HealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::CharacterDead);
	}
	else UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter() - HealthComponent - NULL"));
	
	if (InventoryComponent) {
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}
	else UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter() - Inventory - NULL"));

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	
	//Network
	SetReplicates(true);
}

void ATDSCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	CameraBoom->TargetArmLength = HeightMin;
	CameraBoom->SetRelativeRotation(CameraRotator);
	CameraBoom->bEnableCameraLag = Flow;
	CameraBoom->CameraLagSpeed = SmoothCoef;
}

void ATDSCharacter::BeginPlay() {
	Super::BeginPlay();

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer) {
		if (CrosshairMat && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority) {
			CursorToWorld = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CrosshairMat, CrosshairSize, FVector());
		}
	}
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CursorToWorld) {
		APlayerController* MyController = Cast<APlayerController>(GetController());
		if (MyController && MyController->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			MyController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if (HealthComponent && HealthComponent->IsAlive()) {

		if (GetController() && GetController()->IsLocalPlayerController()) {

			AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
			AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

			if (MovementState == EMovementState::SprintRunState) {
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				
				SetActorRotation(FQuat(myRotator));
				SetActorRotationByYaw_OnServer(myRotator.Yaw);
			}

			APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

			if (MyController) {
				FHitResult ResultHit;
				MyController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

				float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
				SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
				SetActorRotationByYaw_OnServer(FindRotatorResultYaw);

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					float HeightDisplacement = CurrentWeapon->GetWeaponShootingZCoord();
					bool bIsReduceDispersion = false;

					switch (MovementState)
					{
					case EMovementState::AimState:
						if (ResultHit.Location.Z < HeightDisplacement)
							Displacement = FVector(0.0f, 0.0f, HeightDisplacement - ResultHit.Location.Z);
						bIsReduceDispersion = true;
						break;
					case EMovementState::AimWalkState:
						if (ResultHit.Location.Z < HeightDisplacement)
							Displacement = FVector(0.0f, 0.0f, HeightDisplacement - ResultHit.Location.Z);
						bIsReduceDispersion = true;
						break;
					case EMovementState::WalkState:
						if (ResultHit.Location.Z < HeightDisplacement)
							Displacement = FVector(0.0f, 0.0f, HeightDisplacement - ResultHit.Location.Z);
						break;
					case EMovementState::RunState:
						if (ResultHit.Location.Z < HeightDisplacement)
							Displacement = FVector(0.0f, 0.0f, HeightDisplacement - ResultHit.Location.Z);
						break;
					case EMovementState::SprintRunState:
						break;
					default:
						break;
					}

					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
				}
			}
		}
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDSCharacter::InputMouseAxis);

	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), IE_Pressed, this, &ATDSCharacter::WalkEnable);
	NewInputComponent->BindAction(TEXT("MovementModeChangeWalk"), IE_Released, this, &ATDSCharacter::WalkDisable);

	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Pressed, this, &ATDSCharacter::SpeedRunEnable);
	NewInputComponent->BindAction(TEXT("MovementModeChangeSprint"), IE_Released, this, &ATDSCharacter::SpeedRunDisable);

	NewInputComponent->BindAction(TEXT("AimState"), IE_Pressed, this, &ATDSCharacter::AimEnable);
	NewInputComponent->BindAction(TEXT("AimState"), IE_Released, this, &ATDSCharacter::AimDisable);

	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), IE_Released, this, &ATDSCharacter::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("ReloadEvent"), IE_Pressed, this, &ATDSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchWeapon"), IE_Pressed, this, &ATDSCharacter::StartPickUpTimer);
	NewInputComponent->BindAction(TEXT("SwitchWeapon"), IE_Released, this, &ATDSCharacter::TrySwitchWeapon_OnServer);

	NewInputComponent->BindAction(TEXT("DropItemFromInv"), IE_Pressed, this, &ATDSCharacter::StartDropWeaponTimer);
	NewInputComponent->BindAction(TEXT("DropItemFromInv"), IE_Released, this, &ATDSCharacter::ClearDropWeaponTimer);

	NewInputComponent->BindAction(TEXT("Ability"), IE_Pressed, this, &ATDSCharacter::TryUseAbility_OnServer);
}

void ATDSCharacter::InputAxisX(float value)
{
	AxisX = value;
}

void ATDSCharacter::InputAxisY(float value)
{
	AxisY = value;
}

void ATDSCharacter::InputMouseAxis(float value)
{
	MouseAxis = value;
	if (Controller && Controller->IsLocalPlayerController()) {
		CameraSlide(value);
	}
}

void ATDSCharacter::WalkEnable() {
	bWalkEnabled = true;
	ChangeMovementState();
}

void ATDSCharacter::WalkDisable() {
	bWalkEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::SpeedRunEnable() {
	bSprintRunEnabled = true;
	ChangeMovementState();
}

void ATDSCharacter::SpeedRunDisable() {
	bSprintRunEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::AimEnable() {
	bAimEnabled = true;
	ChangeMovementState();
}

void ATDSCharacter::AimDisable() {
	bAimEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::InputAttackPressed() {
	if(HealthComponent && HealthComponent->IsAlive()) AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased() {
	AttackCharEvent(false);
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring) {

	AWeaponDefault* MyWeapon = nullptr;
	MyWeapon = GetCurrentWeapon();

	if (MyWeapon) {
		MyWeapon->SetWeaponFireState_OnServer(bIsFiring);
	}

}

void ATDSCharacter::TryReloadWeapon()
{
	if (HealthComponent && HealthComponent->IsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading) {
		TryReloadWeapon_OnServer();
	}
}

void ATDSCharacter::TryUseAbility_OnServer_Implementation()
{
	if (HealthComponent && HealthComponent->IsAlive() && AbilityEffect) {
		UTDSStateEffects* NewEffect = NewObject<UTDSStateEffects>(this, AbilityEffect);
		if (NewEffect) {
			NewEffect->InitObject(this, FName("spine_02"));
		}
	}
}

void ATDSCharacter::TrySwitchWeapon_OnServer_Implementation(FKey Direction)
{
	//"E" key also used to pickup weapon, so if it held down for time, switch wouldn't execute
	if (HealthComponent && HealthComponent->IsAlive() && PickUpWeaponTimer.IsValid() && GetWorld()) {
		if (GetWorld()->GetTimerManager().GetTimerElapsed(PickUpWeaponTimer) < 0) {
			GetWorld()->GetTimerManager().ClearTimer(PickUpWeaponTimer);
			PickUpWeaponTimer.Invalidate();
			return;
		}
	}

	GetWorld()->GetTimerManager().ClearTimer(PickUpWeaponTimer);
	PickUpWeaponTimer.Invalidate();
	
	if (!CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1) {

		int8 OldIndex = CurrentIndexWeapon;

		if (CurrentWeapon) {
			if (CurrentWeapon->WeaponReloading) {
				CurrentWeapon->CancelReload();
			}
		}
		//prev weapon
		if (InventoryComponent && Direction.ToString() == "E") {
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex)) {

			}
		}//next weapon
		else if (InventoryComponent && Direction.ToString() == "Q") {
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex)) {

			}
		}
	}
}

void ATDSCharacter::InitWeapon(FName WeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentWeaponIndex) {
	
	//OnServer

	if (CurrentWeapon) {
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	
	if (myGI) {
		if (myGI->GetWeaponInfoByName(WeaponName, myWeaponInfo)) {
			if (myWeaponInfo.WeaponClass) {
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon) {

					CurrentWeapon = myWeapon;

					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					
					myWeapon->WeaponSettings = myWeaponInfo;
					myWeapon->WeaponInfo.ClipSize = WeaponAdditionalInfo.ClipSize;
					myWeapon->CurrentWeaponName = WeaponName;

					SetWeaponTypeToAnimInst_Multicast(myWeaponInfo.WeaponType);
					
					myWeapon->WeaponInit();

					CurrentIndexWeapon = NewCurrentWeaponIndex;
				}

				myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
				myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
				myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDSCharacter::WeaponFireStart);
			
				if (CurrentWeapon->GetWeaponClipSize() <= 0 && CurrentWeapon->CanWeaponReload())
					CurrentWeapon->InitReload();

				InventoryComponent->AmmoTypeAvailableEvent_Multicast(myWeapon->WeaponSettings.WeaponType);
			}
		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - Weapon not found in table - NULL"));
		}
	}
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::CameraShake()
{
	
}

void ATDSCharacter::CameraSlide(float AxisValue)
{
	if (SlideDone) {
		if (AxisValue != 0.0f) {
			if (AxisValue < 0) {
				if (CameraBoom->TargetArmLength <= HeightMax) {
					SlideUp = true;
					SlideDone = false;
				}
			}
			else if (AxisValue > 0) {
				if (CameraBoom->TargetArmLength >= HeightMin) {
					SlideUp = false;
					SlideDone = false;
				}
			}
			GetWorldTimerManager().SetTimer(CameraSlideTimer, this, &ATDSCharacter::CameraSlideSmooth, TimerTickValue, true);
		}
	}
}

void ATDSCharacter::CameraSlideSmooth()
{
	if (CameraBoom->TargetArmLength <= HeightMax && CameraBoom->TargetArmLength >= HeightMin) {
		float Step = TickSlideValue;

		if (!SlideUp) Step *= -1;

		CameraBoom->TargetArmLength += Step;
		
		TickCounter += Step;
		if (Step < 0 && CameraBoom->TargetArmLength <= HeightMin) {
			CameraBoom->TargetArmLength = HeightMin;
			TickCounter = HeightChangeCoeff;
		}
		if (Step > 0 && CameraBoom->TargetArmLength >= HeightMax) {
			CameraBoom->TargetArmLength = HeightMax;
			TickCounter = HeightChangeCoeff;
		}

		//TODO Check why abs
		if (abs(TickCounter) >= HeightChangeCoeff) {
			TickCounter = 0.0f;
			SlideDone = true;
			GetWorldTimerManager().ClearTimer(CameraSlideTimer);
		}
	}
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState) {
	case EMovementState::AimState:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalkState:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::WalkState:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::RunState:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRunState:
		ResSpeed = MovementInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	EMovementState NewState = EMovementState::RunState;

	if (!bWalkEnabled && !bSprintRunEnabled && !bAimEnabled) {
		NewState = EMovementState::RunState;
	}
	else {
		if (bSprintRunEnabled) {
			bWalkEnabled = false;
			bAimEnabled = false;
			NewState = EMovementState::SprintRunState;
		}
		else {
			if (bWalkEnabled && !bSprintRunEnabled && bAimEnabled) {
				NewState = EMovementState::AimWalkState;
			}
			else {
				if (bWalkEnabled && !bSprintRunEnabled && !bAimEnabled) {
					NewState = EMovementState::WalkState;
				}
				else {
					if (!bWalkEnabled && !bSprintRunEnabled && bAimEnabled) {
						NewState = EMovementState::AimState;
					}
				}
			}
		}
	}

	SetMovementState_OnServer(NewState);

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();

	if (myWeapon) {
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}
}

EMovementState ATDSCharacter::GetMovementState()
{
	return MovementState;
}

void ATDSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	WeaponFireStart_BP(Anim);
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* Anim, float ReloadTime)
{
	
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon) {
		FVector WeaponSocketLocation = GetMesh()->GetSocketLocation(TEXT("WeaponSocketRightHand"));
		USoundBase* ReloadSound = myWeapon->WeaponSettings.SoundReloadWeapon;

		if (myWeapon->WeaponSettings.SoundReloadWeapon) {
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ReloadSound, WeaponSocketLocation);
		}
	}
	WeaponReloadStart_BP(Anim, ReloadTime);
}

void ATDSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && bIsSuccess && CurrentWeapon) {

		const FString WeaponName = CurrentWeapon->CurrentWeaponName.ToString();
		
		if (CurrentWeapon->GetWeaponType() == EWeaponType::PistolType && WeaponName.Contains(TEXT("_def")))
		{
			InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, 0);
			InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
		}else{
			InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSettings.WeaponType, AmmoTake);
			InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
		}
	}
	WeaponReloadEnd_BP(bIsSuccess, AmmoTake);
}

EPhysicalSurface ATDSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	if (HealthComponent) {
		if (HealthComponent->GetCurrentShield() <= 0.0f) {
			if (GetMesh()) {
				UMaterialInterface* myMat = GetMesh()->GetMaterial(0);
				if (myMat) {
					Result = myMat->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}

	return Result;
}

TArray<UTDSStateEffects*> ATDSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATDSCharacter::RemoveEffect(UTDSStateEffects* ToRemove)
{
	Effects.Remove(ToRemove);
	
	if(!ToRemove->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToRemove, false);
		RemoveEffectRep = ToRemove;
	}
}

void ATDSCharacter::AddEffect(UTDSStateEffects* ToAdd)
{
	Effects.Add(ToAdd);

	if(!ToAdd->IsAutoDestroyParticle()){
		UTypes::SwitchEffect(this, ToAdd, true);
		AddEffectRep = ToAdd;
	}else
	{
		if(ToAdd->GetParticleEffect())
		{
			ExecuteEffectAdded_OnServer(const_cast<UParticleSystem*>(ToAdd->GetParticleEffect()));
		}
	}
}

USceneComponent* ATDSCharacter::GetSceneComponentToAttachEffect()
{	
	return GetMesh();
}

bool ATDSCharacter::GetIsAlive()
{
	bool result = false;
	if(HealthComponent){
		result = HealthComponent->IsAlive();
	}
	return result;
}

void ATDSCharacter::CharacterDead()
{
	if(HasAuthority())
	{
		float AnimTime = 0.0f;
		int32 rnd = FMath::RandHelper(DeadAnims.Num());
		
		if (DeadAnims.IsValidIndex(rnd) && DeadAnims[rnd] && GetMesh() && GetMesh()->GetAnimInstance()) 
		{
			PlayAnim_Multicast(DeadAnims[rnd]);
			AnimTime = DeadAnims[rnd]->GetPlayLength();
		}else UE_LOG(LogTemp, Warning, TEXT("CharacterDead() - error"));

		if (GetController()) {
			GetController()->UnPossess();
		}

		//ragdoll timer
		GetWorldTimerManager().SetTimer(TimerHandle_RagdollActivating, this, &ATDSCharacter::EnableRagdoll_Multicast, AnimTime, false);
	}else
	{
		if(GetCursorToWorld()){
			GetCursorToWorld()->SetVisibility(false);
		}

		AttackCharEvent(false);
	}

	if(GetCapsuleComponent())
	{
		GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
	}
	
	CharacterDead_BP();
}

void ATDSCharacter::CharacterDead_BP_Implementation()
{
	//BP
}

float ATDSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{

	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthComponent && HealthComponent->IsAlive()) {
		HealthComponent->ChangeHealthValue_OnServer(-ActualDamage);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID)) {
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile) {

			//TODO
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSettings.Effect, GetSurfaceType(), NAME_None);
		}
	}

	return ActualDamage;
}

void ATDSCharacter::EnableRagdoll_Multicast_Implementation()
{
	if (GetMesh()){
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}else UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::EnableRagdoll_Multicast() - error"));
}

void ATDSCharacter::StartDropWeaponTimer(FKey KeyPressed)
{
	if (GetWorld()) {
		if (CanDropWeapon()) {
			GetWorld()->GetTimerManager().SetTimer(DropWeaponTimer, this, &ATDSCharacter::DropCurrentWeapon, DropWeaponDelayTime, false);
			StartDropWeaponTimer_BP(DropWeaponTimer, DropWeaponDelayTime, KeyPressed);
		}
	}
}

void ATDSCharacter::ClearDropWeaponTimer()
{
	if (GetWorld())
		GetWorld()->GetTimerManager().ClearTimer(DropWeaponTimer);
	DropWeaponTimer.Invalidate();

	ClearDropWeaponTimer_BP();
}

int32 ATDSCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

void ATDSCharacter::StartDropWeaponTimer_BP_Implementation(FTimerHandle Timer, float Time, FKey KeyPressed)
{
	//BP
}

void ATDSCharacter::ClearDropWeaponTimer_BP_Implementation()
{
	//BP
}

bool ATDSCharacter::CanDropWeapon()
{
	bool bIsCan = false;
	if (CurrentWeapon) {
		if (CurrentWeapon->CurrentWeaponName != FName("Pistol_Def")) {
			bIsCan = true;
		}
	}
	return bIsCan;
}

void ATDSCharacter::DropCurrentWeapon()
{
	if (InventoryComponent) {
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void ATDSCharacter::StartPickUpTimer(FKey KeyPressed)
{
	if (GetWorld()) {
		GetWorld()->GetTimerManager().SetTimer(PickUpWeaponTimer, PickUpWeaponTime, false);
	}
}

void ATDSCharacter::EffectAdd_OnRep()
{
	if(AddEffectRep)
	{
		UTypes::SwitchEffect(this, AddEffectRep, true);
	}
}

void ATDSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATDSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("spine_01"));
}

void ATDSCharacter::EffectRemove_OnRep()
{
	if(RemoveEffectRep)
	{
		UTypes::SwitchEffect(this, RemoveEffectRep, false);
	}
}

void ATDSCharacter::SetWeaponTypeToAnimInst_Multicast_Implementation(EWeaponType NewType)
{
	SetWeaponTypeAnimInst_BP(NewType);
}

void ATDSCharacter::SetWeaponTypeAnimInst_BP_Implementation(EWeaponType NewType)
{
	//in BP
}

void ATDSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* MontageToPlay)
{
	if (GetMesh() && GetMesh()->GetAnimInstance()) {
		GetMesh()->GetAnimInstance()->Montage_Play(MontageToPlay);
	}
}

void ATDSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATDSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController()) {
		SetActorRotation(FQuat(FRotator(0.f, Yaw, 0.f)));
	}
}

void ATDSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATDSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATDSCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponClipSize() < CurrentWeapon->WeaponSettings.MaxClipSize && CurrentWeapon->CanWeaponReload()) {
			CurrentWeapon->InitReload();
	}
}

void ATDSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	//in BP
}

void ATDSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim, float ReloadTime)
{
	// in BP
}

void ATDSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess, int32 AmmoSafe)
{
	// in BP
}

bool ATDSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for(int32 i = 0; i < Effects.Num(); i++)
	{
		if(Effects[i]) { Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags); }
	}
	return Wrote;
}

void ATDSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ATDSCharacter, MovementState);
	DOREPLIFETIME(ATDSCharacter, CurrentWeapon);
	DOREPLIFETIME(ATDSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATDSCharacter, Effects);
	DOREPLIFETIME(ATDSCharacter, AddEffectRep);
	DOREPLIFETIME(ATDSCharacter, RemoveEffectRep);
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "CharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TDS_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()
	
protected:

	float Shield = 100.0f;

public:

	FTimerHandle HealthRegenTimer;
	FTimerHandle HealthRegenRate;

	FTimerHandle ShieldRegenTimer;
	FTimerHandle ShieldRegenRate;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "health")
		FOnShieldChange OnShieldChange;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health Regen")
		float CooldownHealthRestoreTime = 10.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health Regen")
		float HealthRestoreRate = 0.1f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health Regen")
		float HealthRestoreValue = 1.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield Regen")
		float CooldownShieldRestoreTime = 5.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield Regen")
		float ShieldRestoreRate = 0.1f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Shield Regen")
		float ShieldRestoreValue = 1.0f;

	void ChangeHealthValue_OnServer(float Value) override;
	void ChangeShieldValue(float &value);

	float GetCurrentShield();

	void ShieldRecover();
	void CooldownShieldEnd();

	void HealthRecover();
	void CooldownHealthEnd();

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();

	//
	UFUNCTION(NetMulticast, Reliable)
		void ShieldChangeEvent_Multicast(float NewShield, float Value);
	
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"

void UCharacterHealthComponent::ChangeHealthValue_OnServer(float Value)
{
	float CurrentDamage = Value * DamageCoef;

	if (Shield > 0.f && Value < 0.0f) {
		ChangeShieldValue(CurrentDamage);
		if (Shield > 100.f) {
			Shield = 100.f;
		}
		else if (Shield <= 0.f) {
			Super::ChangeHealthValue_OnServer(CurrentDamage);
		}
	}else
		Super::ChangeHealthValue_OnServer(Value);

	if (GetWorld() && Health <= 20.f) {
		GetWorld()->GetTimerManager().SetTimer(HealthRegenTimer, this, &UCharacterHealthComponent::CooldownHealthEnd, CooldownHealthRestoreTime, false);
	}
	GetWorld()->GetTimerManager().ClearTimer(HealthRegenRate);
}

//if caused damage destroy shield, 
//all rest damage goes to health and owerwrite value ref
void UCharacterHealthComponent::ChangeShieldValue(float &value)
{
	Shield += value;

	if (Shield > 100.f)
		Shield = 100.0f;
	else if (Shield <= 0.f) {
		value = Shield;
		Shield = 0.0f;
	}

	if(GetWorld()){
		GetWorld()->GetTimerManager().SetTimer(ShieldRegenTimer, this, &UCharacterHealthComponent::CooldownShieldEnd, CooldownShieldRestoreTime, false);
		GetWorld()->GetTimerManager().ClearTimer(ShieldRegenRate);
	}

	ShieldChangeEvent_Multicast(Shield, value);
}

float UCharacterHealthComponent::GetCurrentShield() {
	return Shield;
}

void UCharacterHealthComponent::ShieldRecover()
{
	float tmp = Shield;
	tmp = tmp + ShieldRestoreValue;

	if (tmp >= 100.0f) {
		Shield = 100.0f;
		if (GetWorld()) {
			GetWorld()->GetTimerManager().ClearTimer(ShieldRegenRate);
			GetWorld()->GetTimerManager().ClearTimer(ShieldRegenTimer);
		}
	}
	else {
		Shield = tmp;
	}	

	ShieldChangeEvent_Multicast(Shield, ShieldRestoreValue);
}

void UCharacterHealthComponent::CooldownShieldEnd()
{
	if (GetWorld()) {
		GetWorld()->GetTimerManager().SetTimer(ShieldRegenRate, this, &UCharacterHealthComponent::ShieldRecover, ShieldRestoreRate, true);
	}
}

//health regenerates below 20%
void UCharacterHealthComponent::HealthRecover()
{
	float tmp = Health;
	tmp = tmp + HealthRestoreValue;

	if (tmp > 20.0f) {
		Health = 20.0f;
		if (GetWorld()) {
			GetWorld()->GetTimerManager().ClearTimer(HealthRegenRate);
			GetWorld()->GetTimerManager().ClearTimer(HealthRegenTimer);
		}
	}
	else {
		Health = tmp;
	}

	HealthChangeEvent_Multicast(Health, HealthRestoreValue);
}

void UCharacterHealthComponent::CooldownHealthEnd()
{
	if (GetWorld()) {
		GetWorld()->GetTimerManager().SetTimer(HealthRegenRate, this, &UCharacterHealthComponent::HealthRecover, HealthRestoreRate, true);
	}
}

float UCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float NewShield, float Value)
{
	OnShieldChange.Broadcast(NewShield, Value);
}

// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

#include "GeneratedCodeHelpers.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicatedByDefault(true);
	// ...
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UHealthComponent::SetCurrentHealth(float value)
{
	Health = value;
}

void UHealthComponent::ChangeHealthValue_OnServer_Implementation(float value)
{
	if(bIsAlive){
		value *= DamageCoef;
		Health += value;

		if (Health > 100.f) {
			Health = 100.f;
		}

		HealthChangeEvent_Multicast(Health, value);

		if (Health <= 0.0f) {
			bIsAlive = false;
			DeadEvent_Multicast();
		}
	}
}

void UHealthComponent::DeadEvent_Multicast_Implementation()
{
	OnDead.Broadcast();
}

void UHealthComponent::HealthChangeEvent_Multicast_Implementation(float NewHealth, float ChangeCount)
{
	OnHealthChange.Broadcast(NewHealth, ChangeCount);
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {

	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
	DOREPLIFETIME(UHealthComponent, bIsAlive);
	
}
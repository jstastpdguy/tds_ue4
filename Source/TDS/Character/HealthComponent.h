// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChange, float, Health, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDead);

USTRUCT(BlueprintType)
struct FStatsParam {
	GENERATED_BODY()
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "health")
	FOnHealthChange OnHealthChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "health")
	FOnDead OnDead;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Health")
	float DamageCoef = 1.f;
	UPROPERTY(Replicated)
	float Health = 100.f;
	UPROPERTY(Replicated)
	bool bIsAlive = true;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BLueprintCallable, BlueprintPure, Category = "Health")
	float GetCurrentHealth();
	UFUNCTION(BLueprintCallable, Category = "Health")
	void SetCurrentHealth(float value);
	UFUNCTION(Server, Reliable, BLueprintCallable, Category = "Health")
	virtual void ChangeHealthValue_OnServer(float Value);
	UFUNCTION(BlueprintCallable, BlueprintPure)
	bool IsAlive() const { return bIsAlive; };

	//Multiplayer events
	UFUNCTION(NetMulticast, Reliable)
	void HealthChangeEvent_Multicast(float NewHealth, float ChangeCount);
	UFUNCTION(NetMulticast, Reliable)
	void DeadEvent_Multicast();
};
// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "../Items/Weapon/ProjectileDefault.h"
#include "../Items/Weapon/TDSStateEffects.h"
#include "../Interface/TDS_I_GameActors.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDSStateEffects> AddEffectClass, EPhysicalSurface SurfaceType, FName HittedBone) {
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass) {

		UTDSStateEffects* myEffect = Cast<UTDSStateEffects>(AddEffectClass->GetDefaultObject());

		if (myEffect) {

			const TArray<TEnumAsByte<EPhysicalSurface>>* PossibleInteractSurface = myEffect->GetPossibleInteractSurface();
			
			for (auto i : *PossibleInteractSurface) {

				if (i == SurfaceType) {

					if (!myEffect->IsStackable()) {
						TArray<UTDSStateEffects*> CurrentEffects;
						ITDS_I_GameActors* myInterface = Cast<ITDS_I_GameActors>(TakeEffectActor);
						if (myInterface) {
							CurrentEffects = myInterface->GetAllCurrentEffects();
							for (auto j : CurrentEffects) {
								if (j->GetClass() == AddEffectClass) return;
							}
						}
					}

					UTDSStateEffects* NewEffect = NewObject<UTDSStateEffects>(TakeEffectActor, AddEffectClass);
					if (NewEffect) {
						NewEffect->InitObject(TakeEffectActor, HittedBone);
					}
					else UE_LOG(LogTemp, Warning, TEXT("UTypes::AddEffectBySurfaceType() - NewObject - failed"));

					break;
				}
			}
		}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket)
{
	if(Target)
	{
		FName SocketToAttach = Socket;
		FVector Loc = Offset;
		ITDS_I_GameActors* MyInterface = Cast<ITDS_I_GameActors>(Target);

		if(MyInterface)
		{
			USceneComponent* SceneToAttach = MyInterface->GetSceneComponentToAttachEffect();
			if(SceneToAttach)
			{
				UGameplayStatics::SpawnEmitterAttached(ExecuteFX, SceneToAttach, SocketToAttach, Offset, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);
			}
		}
	}
}

void UTypes::SwitchEffect(AActor* Target, UTDSStateEffects* Effect, bool IsAdd)
{
	ITDS_I_GameActors* MyInterface = Cast<ITDS_I_GameActors>(Target);

	if(MyInterface)
	{
		const FVector Loc = MyInterface->GetEffectOffset();
		FName* Bone = const_cast<FName*>(Effect->GetBoneName());
		USceneComponent* SceneToAttach = MyInterface->GetSceneComponentToAttachEffect();
		TArray<UParticleSystemComponent*>* ParticleComponents = MyInterface->GetAllParticleComponents();

		if(IsAdd)
		{
			if(Effect && Effect->GetParticleEffect())
			{
				if(SceneToAttach)
				{
					UParticleSystem* Particle = const_cast<UParticleSystem*>(Effect->GetParticleEffect());
					UParticleSystemComponent* NewParticleSystemComponent = UGameplayStatics::SpawnEmitterAttached(Particle, SceneToAttach, *Bone, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTargetIncludingScale, false);
					ParticleComponents->Add(NewParticleSystemComponent);
				}
			}
		}else
		{
			if(ParticleComponents->Num()){
				for(auto i : *ParticleComponents)
				{
					if(i->Template && Effect->GetParticleEffect() && Effect->GetParticleEffect() == i->Template)
					{
						i->DeactivateSystem();
						i->DestroyComponent();
						ParticleComponents->Remove(i);
						break;
					}
				}
			}
		}
	}
}

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "TDS/Items/Weapon/TDSStateEffects.h"
#include "Types.generated.h"

class ITDS_I_GameActors;
UENUM(BlueprintType)
enum class EMovementState : uint8 {
	AimState UMETA(DisplayName = "Aim State"),
	AimWalkState UMETA(DisplayName = "AimWalk State"),
	WalkState UMETA(DisplayName = "Walk State"),
	RunState UMETA(DisplayName = "Run State"),
	SprintRunState UMETA(DisplayName = "SprintRun State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "Shot Gun"),
	SniperRifle UMETA(DisplayName = "Sniper Rifle"),
	GrenadeLauncher UMETA(DisplayName = "Grenade Launcher"),
	RocketLauncher UMETA(DisplayName = "Rocket Launcher"),
	PistolType UMETA(DisplayName = "Pistol")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintRunSpeedRun = 800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RollState = 7000.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Classes")
		TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Meshes")
		class UStaticMesh* BulletMesh = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Fire Settings")
		float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Fire Settings")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Fire Settings")
		float ProjectileInitSpeed = 2000.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Fire Settings")
		float ProjectileMaxSpeed = 2000.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
		class UParticleSystem* BulletFX = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
		FTransform FXOffset = FTransform();
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* HitSound = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Effects")
		TSubclassOf<class UTDSStateEffects> Effect = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		UParticleSystem* ExplosionFX = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		USoundBase* ExplosionSound = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		float ExplosionMaxDamage = 40.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		bool bShouldExplodeOnHit = false;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		float ExplosionInnerRadius = 50.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		float ExplosionOuterRadius = 200.f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Explosion Settings")
		float ExplosionDamageFalloffCoeff = 0.f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimState_DispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimState_DispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimState_DispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimState_DispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalkState_DispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalkState_DispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalkState_DispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalkState_DispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float WalkState_DispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float WalkState_DispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float WalkState_DispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float WalkState_DispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float RunState_DispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float RunState_DispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float RunState_DispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float RunState_DispersionReduction = 0.1f;
};

USTRUCT(BlueprintType)
struct FShootingAnimations {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
	UAnimMontage* AnimCharReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* AnimWeaponReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* AnimWeaponFire = nullptr;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
	UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh Time")
	float DropMeshTime = -1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh Time")
	float DropMeshLifeTime = 5.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity Settings")
	FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity Settings")
	FVector DropMeshImpulseDir = FVector::ZeroVector;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity Settings")
	float PowerImpulse = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity Settings")
	float ImpulseRandomDispersion = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Velocity Settings")
	float CustomMass = 0.f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase {

	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Classes")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	int32 MaxClipSize = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	FProjectileInfo ProjectileSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fire Settings")
	float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	EWeaponType WeaponType = EWeaponType::PistolType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float SwitchTimeToWeapon = 1.0f; //REMOVE
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
	FShootingAnimations ShootAnims;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshInfo ClipDrop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshInfo BulletShellDrop;
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo {
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 ClipSize = 10;
};

USTRUCT(BlueprintType)
struct FWeaponSlot {
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon Slot")
	FName NameItem;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Weapon Slot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot {
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ammo Slot")
	EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ammo Slot")
	int32 Count = 100;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Ammo Slot")
	int32 MaxCount = 100;
};

USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Weapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Weapon")
	USkeletalMesh* WeaponSkeletalMesh = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Weapon")
	UParticleSystem* FX = nullptr;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Weapon")
	FWeaponSlot WeaponInfo;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Drop Weapon")
	FTransform Offset;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary {
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<class UTDSStateEffects> AddEffectClass, EPhysicalSurface SurfaceType, FName HittedBone);
	UFUNCTION(BlueprintCallable)
	static void ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* Target, FVector Offset, FName Socket);
	UFUNCTION(BlueprintCallable)
	static void SwitchEffect(AActor* Target, UTDSStateEffects* Effect, bool IsAdd);
};
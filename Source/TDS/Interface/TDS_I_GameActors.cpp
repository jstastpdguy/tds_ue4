// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_I_GameActors.h"
#include "TDS/Items/Weapon/TDSStateEffects.h"

// Add default functionality here for any ITDS_I_GameActors functions that are not pure virtual.

EPhysicalSurface ITDS_I_GameActors::GetSurfaceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

TArray<UTDSStateEffects*> ITDS_I_GameActors::GetAllCurrentEffects()
{
	return TArray<UTDSStateEffects*>();
}

void ITDS_I_GameActors::RemoveEffect(UTDSStateEffects* Remove)
{
}

void ITDS_I_GameActors::AddEffect(UTDSStateEffects* ToAdd)
{
}

FVector ITDS_I_GameActors::GetEffectOffset()
{
	return FVector::ZeroVector;
}

USceneComponent* ITDS_I_GameActors::GetSceneComponentToAttachEffect()
{
	return nullptr;
}

TArray<UParticleSystemComponent*>* ITDS_I_GameActors::GetAllParticleComponents()
{
	TArray<UParticleSystemComponent*>* Particles = new TArray<UParticleSystemComponent*>();
	return Particles;
}

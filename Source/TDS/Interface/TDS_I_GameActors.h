// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../Items/Weapon/TDSStateEffects.h"
#include "../FuncLibrary/Types.h"
#include "TDS_I_GameActors.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_I_GameActors : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDS_I_GameActors
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UTDSStateEffects*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDSStateEffects* ToRemove);
	virtual void AddEffect(UTDSStateEffects* ToAdd);
	virtual FVector GetEffectOffset();
	virtual USceneComponent* GetSceneComponentToAttachEffect();
	virtual TArray<UParticleSystemComponent*>* GetAllParticleComponents();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Count);
};
